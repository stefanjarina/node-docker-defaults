// modules imports
const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const mongoose = require('mongoose')
const Promise = require('bluebird')

// config imports
const config = require('./config/app')
const db = require('./config/db')

// Create an express application instnace
const app = express()

// availability monitoring
app.get('/ping', (req, res) => {
  res.send('pong')
})

// middleware
app.use(morgan('combined')) // HTTP request logger middleware
app.use(bodyParser.json()) // JSON Parser middleware
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride())
app.use(helmet()) // Protection headers
app.use(cors()) // CORS middleware

// Connect to Mongoose and use Bluebird promises
mongoose.Promise = Promise
mongoose.connect(db.url, {
  useMongoClient: true
})
  .then(() => {})
  .catch((err) => {
    console.error(err.message)
    process.exit()
  })

// Configure CORS protection
app.all('/*', (req, res, next) => {
  // CORS headers
  res.header('Access-Control-Allow-Origin', '*') // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key')
  if (req.method === 'OPTIONS') {
    res.status(200).end()
  } else {
    next()
  }
})

// Routes
app.use('/api/v1', require('./app/routes'))

// If no path found by now, return error
app.use(function (req, res, next) {
  let err = new Error('Not Found')
  err.code = 404
  next(err)
})

// Error interceptor (handle errors at one place)
app.use(function (err, req, res, next) {
  if (err.code === 401) {
    return res.redirect('/login')
  }
  if (err.code === 403) {
    console.error(err.message)
    if (err.msg) {
      err.message = err.msg
    } else {
      err.message = "I'm sorry, you don't have the sufficient rights to access the requested resource"
    }
    res.status(403)
    return res.send({error: err.message})
  }
  if (err.code === 404) {
    res.status(404)
    console.error(err.message)
    return res.send({
      error: 'Resource Not Found'
    })
  }
  res.status(500)
  console.error(err.message)
  return res.send({
    error: 'Internal Server Error'
  })
})

// start app ========================================
const server = app.listen(config.port, () => {
  let port = config.port === '80' ? '' : config.port
  console.log('Magic happens on http://localhost/' + port) // shoutout to the user
}) // startup and bind to port

//
// need this in docker container to properly exit since node doesn't handle SIGINT/SIGTERM
// this also won't work on using npm start since:
// https://github.com/npm/npm/issues/4603
// https://github.com/npm/npm/pull/10868
// https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
// if you want to use npm then start with `docker run --init` to help, but I still don't think it's
// a graceful shutdown of node process
//

// quit on ctrl-c when running docker in terminal
process.on('SIGINT', function onSigint () {
  console.info('Got SIGINT (aka ctrl-c in docker). Graceful shutdown ', new Date().toISOString())
  shutdown()
})

// quit properly on docker stop
process.on('SIGTERM', function onSigterm () {
  console.info('Got SIGTERM (docker container stop). Graceful shutdown ', new Date().toISOString())
  shutdown()
})

// shut down server
function shutdown () {
  server.close(function onServerClosed (err) {
    if (err) {
      console.error(err)
      process.exitCode = 1
    }
    process.exit()
  })
}
//
// need above in docker container to properly exit
//
