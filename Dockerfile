FROM node:8-alpine

RUN mkdir -p /opt/app

# defaults to production, docker-compose overrides this to development
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# defaults to port 80 for node and 5858 or 9229 for debug
ARG PORT=80
ENV PORT $PORT
EXPOSE $PORT 5858 9229

# check every 30s to ensure this service returns HTTP 200
HEALTHCHECK CMD curl -fs http://localhost:$PORT/ping || exit 1

# install dependencies first, in a different location for easier app bind mounting for local development
WORKDIR /opt
COPY package.json /opt
RUN npm install && npm cache clean --force
ENV PATH /opt/node_modules/.bin:$PATH

# source code changes often so copy it last
WORKDIR /opt/app
COPY . /opt/app

CMD [ "node", "src/app.js" ]
