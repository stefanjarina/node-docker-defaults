const Bookmark = require('../models/bookmark')

module.exports = {
  // GET /bookmarks
  index (req, res) {
    Bookmark.find({})
      .then((result) => {
        res.send(result)
      })
      .catch((err) => {
        res.send({error: err.message})
      })
  },

  // GET /bookmarks/:id
  show (req, res, next) {
    const id = req.params.id
    Bookmark.findOne({
      _id: id
    })
      .then((bookmark) => {
        res.send(bookmark)
      })
      .catch((err) => {
        if (err.name === 'CastError') {
          err.code = 404
          return next()
        }
        res.send({error: err.message})
      })
  },

  // POST /bookmarks
  create (req, res, next) {
    const bookmark = req.body
    Bookmark.create(bookmark)
      .then((result) => {
        res.send(result)
      })
      .catch((err) => {
        next(err)
      })
  },

  // PUT /bookmarks/:id
  update (req, res, next) {
    const id = req.params.id
    const bookmark = req.body
    Bookmark.findByIdAndUpdate(id, {
      $set: bookmark
    }, { new: true })
      .then((result) => {
        res.send(result)
      })
      .catch((err) => {
        if (err.name === 'CastError') {
          err.code = 404
          return next(err)
        }
        next(err)
      })
  },

  // DELETE /bookmarks/:id
  remove (req, res, next) {
    const id = req.params.id
    Bookmark.remove({ _id: id })
      .then(() => {
        res.send({ action: 'successful' })
      })
      .catch((err) => {
        if (err.name === 'CastError') {
          err.code = 404
          return next(err)
        }
        next(err)
      })
  }

}
