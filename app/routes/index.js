const express = require('express')
const router = express.Router()

// routes imports
const bookmarks = require('./bookmarks')

// Bookmarks resource
router.get('/bookmarks', bookmarks.index)
router.get('/bookmarks/:id', bookmarks.show)
router.post('/bookmarks', bookmarks.create)
router.put('/bookmarks/:id', bookmarks.update)
router.delete('/bookmarks/:id', bookmarks.remove)

module.exports = router
