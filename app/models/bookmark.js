const mongoose = require('mongoose')

const Schema = mongoose.Schema

const BookmarkSchema = new Schema({
  name: String,
  description: String,
  url: String
}, {timestamps: true})

module.exports = mongoose.model('Bookmark', BookmarkSchema)
